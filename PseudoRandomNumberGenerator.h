//==============================================================================
// include guard
#ifndef __PSERANNUMGEN_H_INCLUDED__
#define __PSERANNUMGEN_H_INCLUDED__

//==============================================================================
// forward declared dependencies

//==============================================================================
// included dependencies
#include <iostream>
#include <chrono>
#include <random>
//==============================================================================

class PseudoRandomNumberGenerator{
	
	unsigned Seed;
	// mt19937 is a standard mersenne_twister_engine
	std::mt19937 pRNG_mt19937;
	float inv_max;
public:
	//--------------------------------------------------------------------------
	PseudoRandomNumberGenerator(){
		set_seed_now_using_system_clock();
		inv_max = 1.0 / float(pRNG_mt19937.max());
	};
	//--------------------------------------------------------------------------
	void set_seed_now_using_system_clock() {
		Seed = std::chrono::system_clock::now().time_since_epoch().count();
		pRNG_mt19937.seed(Seed);
	};
	//--------------------------------------------------------------------------
	float uniform() {
		return float(pRNG_mt19937())/float(pRNG_mt19937.max());
	};
	//--------------------------------------------------------------------------
	int uniform_int_from_0_to(const uint max_range) {
		return int( float(pRNG_mt19937())*inv_max*max_range);
	}
	//--------------------------------------------------------------------------
	unsigned seed()const{
		return Seed;
	};
	//--------------------------------------------------------------------------
	void set_seed(unsigned new_Seed){
		pRNG_mt19937.seed(new_Seed);
	};
	//--------------------------------------------------------------------------
};
#endif // __PSERANNUMGEN_H_INCLUDED__