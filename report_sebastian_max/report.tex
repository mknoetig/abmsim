\documentclass[a4paper, 10pt]{article} 
\usepackage[protrusion=true,expansion=true]{microtype}
\usepackage[onehalfspacing]{setspace}
\usepackage{graphicx} 
\usepackage{wrapfig}
\usepackage{float}
\usepackage{mathpazo}
\usepackage{amsmath}
\usepackage[T1]{fontenc}
\linespread{1.05} 
\usepackage{listings}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{setspace}

\makeatletter
\renewcommand\@biblabel[1]{\textbf{#1.}} 
\renewcommand{\@listI}{\itemsep=0pt}
\renewcommand{\maketitle}{
	\begin{flushright} 
		{\LARGE\@title}
		\vspace{50pt}
		{\large\@author}
		\\\@date
		\vspace{40pt}
	\end{flushright}
}
\newcommand{\SideBySide}[2]{
	\newline
	\begin{minipage}[t]{0.485\linewidth}
		\vspace{-0.5cm}
		#1
		\end{minipage}
	%
	\hfill
	%
		\begin{minipage}[t]{0.485\linewidth}
		\vspace{-0.5cm}
		#2
		\vspace{0.25cm}
	\end{minipage}\\
}
\newcommand{\AlignmentTitel}[1]{
	\title{\textbf{#1}\\
	\vspace{1cm}
	Probabilistic linear voter model \\ neighborhood investigations \\}
	\date{\today}
}
\newcommand{\CenFig}[3]{
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=1\textwidth]{#1}
            \caption[]{#2}
            \label{#3}
        \end{center}
    \end{figure}
}
\newcommand{\CenFigPoint}[3]{
    \begin{figure}[H]
        \begin{center}
            \includegraphics[width=0.9\textwidth]{#1}
            \caption[]{#2}
            \label{#3}
        \end{center}
    \end{figure}
}
\author{\textsc{Sebastian M\"uller, Max Ahnen}} % Institution

%------------------------------------------------------------------------------
\AlignmentTitel{
	Agent-based modelling --- project report
}
%------------------------------------------------------------------------------
\begin{document}
\onehalfspacing
\maketitle
\rule{\textwidth}{.4pt}
\begin{abstract}
%
	In the course Agent-based modeling of FS2015 at ETH Zurich, we completed an independent agent-based project in the scope of the lecture.
	%
	In this study, we investigate extensions of the linear probabilistic voter model.
	%
	Motivated by social human interactions which contain not only local, but also random encounters, we study extensions to more complex neighborhoods.
	%
	We investigate round and square local-neighborhoods as well as mixtures with random-neighborhoods.
	%
	Also we investigate different geometric initial arrangements.
	%
	The goal is to influence the time-to-dominance and to find out if it is possible break dominance in this way --- the main result of the standard probabilistic linear voter model.
	%
	We develop a flexible computer model implementation where our agents live on the 2D surface of a quadratic torus.
	With this model we investigate the parameter space. 
	%
	The main finding is a negative result, i.e. no change in the neighborhood size, shape, randomness or initialization could prevent dominance.
	%
	In order to break dominance, nonlinear behavior is needed.
	%
	However, a small but significant impact on the time-to-dominance can be inferred.
%
\end{abstract}
%------------------------------------------------------------------------------
\section{Introduction}
%
Our team started off as four students: Alexandre Caron, Patrik Friedlos, Sebastian M\"uller and Max Ahnen.
After discussing and discarding many ideas of too complex agent based simulations, we decided to explore extensions to the probabilistic linear voter model, as shown in the lecture.
%
Together, we four developed the basic probabilistic linear voter model framework for the investigations.
Although we were using three different operating systems in our group of four students, we manged to get started quickly by using a remotely hosted version control program (Git on Bitbucket).
%
Our project sources, written in Python and C++, can be found on Bitbucket \footnote{down-loadable from: https://bitbucket.org/mknoetig/abmsim}.\\
Motivated by social interaction behavior we came up wih two simple extensions to the probabilistic linear voter model.
%
First, motivated by Daniel Kahneman \cite{b:kahneman}, the group of Alexandre and Patrik went for the effect of humans overestimating low probabilities, which is a fundamental pillar in gambling industries. Such \textbf{non linear response} can be explored with minor modifications and therefore looked promising. 
The report from Alexandre and Patrik can be found elsewhere.
%
Second, inspired by the social network of humans, the group of Max and Sebastian started to modify the notion of \textbf{neighborhood}. This is the topic of this report.\\
Human relations do not only have local neighborhoods but also a random ones. 
Here the local neighborhood may be identified with close friends, partners and colleagues. However, this local neighborhood seemed unsufficient.
We wanted to include a random neighborhood as one meets other people randomly, e.g. in pubs or public transportation.
%
This extension is implemented by adding a fraction of random agents to the local neighborhood.\\
%
Furthermore, we were interested in the impact of different geometrical initial states on the 2D grid.
%
This extension is motivated by the forming of urban clusters which might influence opinion propagation by their shapes and borders.
%
The main number investigated is the time-to-dominance, i.e. the number of iterations steps until only a single opinion exists on the 2D grid of agents.
%
%------------------------------------------------------------------------------
\section{Model Implementation}
%
Our agents live in the following world:
%
\begin{itemize}
	\item A quadratic 2D torus with edge length 3 to 30.
	\item Combined neighborhood: local + random
	\item Local neighborhood: quadratic (Moore) or round (Neumann), both with variable radius
	\item Random neighborhood: random agents from anywhere on the grid
	\item Opinion initialization: always 50\%/50\%. 
	\item Initialization shapes:  circular (central cluster), half-half (one hemisphere only), and random
\end{itemize}
%
The agents interact according to the probabilistic linear voter model. The implementation follows 
the formulas, explained in \cite{b:abmsim_linear_voter}.
%
Every time step the following happens:
%
\begin{itemize}
	\item An agent is randomly chosen
	\item The transition rate $w(1-s_i|s_i)$ from the frequency of opinions in the combined neighborhood is calculated for the chosen agent
	\item The chosen agent either keeps or changes its opinion by comparing its transition rate $w$ and a random number uniformly drawn from [0,1]
\end{itemize}
%
The implementation features a terminal output for possible graphical visualization, see figure \ref{FterminalOutput}.
The program returns after dominance is established. 
%
Using UNIX-like program options, the simulation is called with a set of desired parameters. 
%
It returns the number of time steps needed to dominance (time-to-dominance). 
%
\begin{figure}[H]
    \begin{center}
        \begin{footnotesize}
        	\setstretch{1.0}
			\lstset{
				frame=single,
				language=bash,
    			basicstyle=\tiny\color{black}\ttfamily
			}
			\lstinputlisting{python_exploration/terminal_output.txt}
		\end{footnotesize}
        \caption[]{Output of the simulation, grid of size $50x 50$. Different opinions are encoded with either $CD$ or whitespace.}
        \label{FterminalOutput}
    \end{center}
\end{figure}
%------------------------------------------------------------------------------
\section{Results}
%
In order to obtain significant results we run each parameter configuration 100 times and extract the mean time-to-dominance as well its estimated statistical error. Statistical confidence indicators are provided in our figures by using error bars. In total $\approx 3.3\cdot 10^9$ iteration steps have been performed.
%
In particular we investigate in three directions:
%
\begin{itemize}
	\item initialization of agent opinions, cf. figure \ref{fig:1} 
	\item growth of random neighborhood, cf. figure \ref{fig:2}
	\item shape of local neighborhood, cf. figure \ref{fig:3}
\end{itemize}
%
We study each of these for varying edge length of the torus, starting with 3 up to 30, i.e. 9 up to 900 Agents.
%
\CenFigPoint{python_exploration/1.jpg}{
	Different initial, geometric states of opinion distribution on the 2D grid.
}{fig:1}
%
\CenFigPoint{python_exploration/3.jpg}{
	Different sizes of the random neighborhood starting from none to four.  
}{fig:2}
%
\CenFigPoint{python_exploration/5.jpg}{
	Different local neighborhood sizes and shapes.
}{fig:3}
\newpage
We find that:
\begin{itemize}
	\item The different geometric initialization shapes (circular, half-half, random) have no measurable effect on the time-to-dominance, see figure \ref{fig:1}.
	\item An increasing random neighborhood significantly decreases the time-to-dominance, but only little, see figure \ref{fig:2}.
	\item The increasing local neighborhood significantly decreases the time-to-dominance, but also only little, see figure \ref{fig:3}.
\end{itemize}
%
\section{Discussion and Conclusion}
%
The main finding is a negative result, i.e. no change in the neighborhood size, shape, randomness or initialization could break the dominance.
%
This findings agree well with analytical studies of the probabilistic linear voter model \cite{b:schweizer_non_linear}.
Both, the simulations and the analytical results, show that non-linear responses of the agents are needed in order to break dominance. 
%
However, we find a small but significant decrease in time-to-dominance when either random neighborhood is increased or local neighborhood is increased.
%
%------------------------------------------------------------------------------
%	BIBLIOGRAPHY
\bibliographystyle{unsrt}
\bibliography{references}
%------------------------------------------------------------------------------
\end{document}