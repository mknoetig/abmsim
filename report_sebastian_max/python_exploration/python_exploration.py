#!/usr/bin/env python2
# -*- coding:utf-8 -*-
"""
Submitter for the agent based modelling project jobs

"""
import subprocess
from os import path
import time
import shlex
import os
import sys
import glob
import numpy as np
import matplotlib.pyplot as plt

#check how the time-to-dominance changes with respect to the grid size
edges = np.arange(3,31) #try first only from 3 to 30 edge size
attempts = 100
data1 = np.zeros((edges.__len__(),attempts))
data2 = np.zeros((edges.__len__(),attempts))
data3 = np.zeros((edges.__len__(),attempts))

data4 = np.zeros((edges.__len__(),attempts))
data5 = np.zeros((edges.__len__(),attempts))
data6 = np.zeros((edges.__len__(),attempts))
data7 = np.zeros((edges.__len__(),attempts))
data8 = np.zeros((edges.__len__(),attempts))

data9 = np.zeros((edges.__len__(),attempts))
data10 = np.zeros((edges.__len__(),attempts))
data11 = np.zeros((edges.__len__(),attempts))
data12 = np.zeros((edges.__len__(),attempts))
data13 = np.zeros((edges.__len__(),attempts))
data14 = np.zeros((edges.__len__(),attempts))

#check effect of different initializations
for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data1[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=halfhalf --nMask=circle --nRadius=1 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data2[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data3[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

#check effect of random neighbors on time to dominance
for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data4[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=1 --gSize=%d" % edge_len
		
		#excecute command
		data5[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=2 --gSize=%d" % edge_len
		
		#excecute command
		data6[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=3 --gSize=%d" % edge_len
		
		#excecute command
		data7[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=4 --gSize=%d" % edge_len
		
		#excecute command
		data8[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

#check effect of different neighborhood on time to dominance
for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=1 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data9[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=2 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data10[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=circle --nRadius=3 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data11[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=square --nRadius=1 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data12[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=square --nRadius=2 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data13[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])

for i,edge_len in enumerate(edges):
	for j in range(attempts):
		
		print("-------------------new job------------------")
		print("job %d out of %d, total %2.2f Percent done"%((j+1)+i*attempts,edges.__len__()*attempts,100.*(float((j+1)+i*attempts))/float(edges.__len__()*attempts)))
		
		command = "../build/abmsim --init=random --nMask=square --nRadius=3 --nRandom=0 --gSize=%d" % edge_len
		
		#excecute command
		data14[i,j] = int((subprocess.check_output(shlex.split(command)).split(","))[0])




plt.clf()
plt.figure()
# add some text for labels, title and axes ticks
plt.errorbar(edges,np.mean(data1,axis=1),np.std(data1,axis=1)/np.sqrt(100.0), label="random init")
plt.errorbar(edges,np.mean(data2,axis=1),np.std(data2,axis=1)/np.sqrt(100.0), label="halfhalf init")
plt.errorbar(edges,np.mean(data3,axis=1),np.std(data3,axis=1)/np.sqrt(100.0), label="circle init")
plt.title("<time> to domination, circle nbh. r=1, no random")
plt.xlabel("edge length (of the torus)")
plt.ylabel("time")
plt.legend(loc="lower right")
plt.semilogy()

plt.figure()
# add some text for labels, title and axes ticks
plt.errorbar(edges,np.mean(data4,axis=1),np.std(data4,axis=1)/np.sqrt(100.0), label="random nbh. 0")
plt.errorbar(edges,np.mean(data5,axis=1),np.std(data5,axis=1)/np.sqrt(100.0), label="random nbh. 1")
plt.errorbar(edges,np.mean(data6,axis=1),np.std(data6,axis=1)/np.sqrt(100.0), label="random nbh. 2")
plt.errorbar(edges,np.mean(data7,axis=1),np.std(data7,axis=1)/np.sqrt(100.0), label="random nbh. 3")
plt.errorbar(edges,np.mean(data8,axis=1),np.std(data8,axis=1)/np.sqrt(100.0), label="random nbh. 4")
plt.title("<time> to domination, circle nbh. r=1, random init.")
plt.xlabel("edge length (of the torus)")
plt.ylabel("time")
plt.legend(loc="lower right")
plt.semilogy()

plt.figure()
# add some text for labels, title and axes ticks
plt.errorbar(edges,np.mean(data9,axis=1),np.std(data9,axis=1)/np.sqrt(100.0), label="circle nbh. r=1")
plt.errorbar(edges,np.mean(data10,axis=1),np.std(data10,axis=1)/np.sqrt(100.0), label="circle nbh. r=2")
plt.errorbar(edges,np.mean(data11,axis=1),np.std(data11,axis=1)/np.sqrt(100.0), label="circle nbh. r=3")
plt.errorbar(edges,np.mean(data12,axis=1),np.std(data12,axis=1)/np.sqrt(100.0), label="square nbh. r=1")
plt.errorbar(edges,np.mean(data13,axis=1),np.std(data13,axis=1)/np.sqrt(100.0), label="square nbh. r=2")
plt.errorbar(edges,np.mean(data14,axis=1),np.std(data14,axis=1)/np.sqrt(100.0), label="square nbh. r=3")
plt.title("<time> to domination, random nbh. 0, random init.")
plt.xlabel("edge length (of the torus)")
plt.ylabel("time")
plt.legend(loc="lower right")
plt.semilogy()
plt.show()
