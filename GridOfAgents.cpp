#include "GridOfAgents.h"

GridOfAgents::GridOfAgents(const uint _edge_length): edge_length(_edge_length) {
	init_agents_on_grid();
	prng.set_seed(0);
}

Agent* GridOfAgents::get_agent_at(const GridPosition pos) {
	return grid[periodic_boundary(pos.x())][periodic_boundary(pos.y())];
}

Agent* GridOfAgents::get_random_agent() {
	return get_agent_at(get_random_position_on_grid());
}

void GridOfAgents::insert_local_neighbours_to_combined(
	GridPosition &agent_position,
	Agent** &combined_neighbourhood
) {
	uint i = 0;
	for(GridPosition neighbour_pos : local_neighbourhood_mask) {
		combined_neighbourhood[i] = get_agent_at(neighbour_pos+agent_position);
		i++;
	}
}

void GridOfAgents::insert_random_neighbourhood(
	Agent** &combined_neighbourhood
) {
	for(uint i=local_neibor_size; i<local_neibor_size+rand_neibor_size; i++) {
		combined_neighbourhood[i] = 
			get_agent_at(get_random_position_on_grid());
	}	
}

std::vector<GridPosition> GridOfAgents::get_all_positions()const {
	
	std::vector<GridPosition> positions;
	for(uint row=0; row<edge_length; row++) {
		for(uint col=0; col<edge_length; col++) {
			GridPosition pos(row,col);
			positions.push_back(pos);
		}
	}
	return positions;
}

GridPosition GridOfAgents::get_random_position_on_grid() {
	return GridPosition(
		prng.uniform_int_from_0_to(edge_length),
		prng.uniform_int_from_0_to(edge_length)
	);
}

std::string GridOfAgents::get_print()const {
	std::stringstream out;
	out << "_Grid_of_agents_" << edge_length << "_x_" << edge_length << "_=_";
	out <<  edge_length*edge_length*1e-6 << "M\n   ";

	for(uint col=0; col<edge_length; col=col+2) {
		if(col < 10)
			out << " " << col << "  ";
		else
			out << col << "  ";		
	}
	out << "\n   ";
	for(uint col=1; col<edge_length; col=col+2) {
		if(col < 10)
			out << "   " << col;
		else
			out << "  " << col;		
	}
	out << "\n";

	for(uint row=0; row<edge_length; row++) {
		if(row < 10)
			out << " " << row << " ";
		else
			out << row << " ";

		for(uint col=0; col<edge_length; col++) {
			if(grid[row][col]->get_opinion() == 1)
				out << "CD";
			else
				out << "  ";
		}
		out << "\n";
	}
	return out.str();
}

double GridOfAgents::get_opinion_fraction() {
	double opinion=0;

	for(uint row=0; row<edge_length; row++) {
		for(uint col=0; col<edge_length; col++) {
			if(grid[row][col]->get_opinion() == 1)
				opinion+=1.;
		}
	}
	return opinion / edge_length / edge_length;
}

std::ostream& operator<<(std::ostream& os, const GridOfAgents& goa){
	 	os << goa.get_print();
	return os;
}

int GridOfAgents::periodic_boundary(const int pos)const {
	return pos%edge_length;
}

void GridOfAgents::init_agents_on_grid() {
	grid = new Agent** [edge_length];
	for(uint row=0; row<edge_length; row++) 
		grid[row] = new Agent* [edge_length];

	for(uint row=0; row<edge_length; row++)
		for(uint col=0; col<edge_length; col++)
			grid[row][col] = new Agent;
}

void GridOfAgents::toggle_opinion_of_agent_at(const GridPosition pos) {
	Agent* agent = get_agent_at(pos);
	agent->set_opinion(1 - agent->get_opinion());
}

void GridOfAgents::init_random_fraction(const int seed, const double OnFraction) {
	PseudoRandomNumberGenerator prng;
	prng.set_seed(seed);

	for(uint row=0; row<edge_length; row++) {
		for(uint col=0; col<edge_length; col++) {
			if(  OnFraction >= prng.uniform() )
				grid[row][col]->set_opinion(1);
			else
				grid[row][col]->set_opinion(0);
		}
	}	
}

void GridOfAgents::init_half_falf() {
	for(uint row=0; row<edge_length; row++) {
		for(uint col=0; col<edge_length; col++) {
			if(row < edge_length/2)
				grid[row][col]->set_opinion(1);
			else
				grid[row][col]->set_opinion(0);
		}
	}	
}

void GridOfAgents::init_circle() {
	float half_grid = float(edge_length)/2.0;
	GridPosition center(half_grid,half_grid);

	for(uint row=0; row<edge_length; row++) {
		for(uint col=0; col<edge_length; col++) {

			GridPosition pos(row,col);

			if((pos - center).norm() <= half_grid*0.75)
				grid[row][col]->set_opinion(1);
			else
				grid[row][col]->set_opinion(0);
		}
	}	
}