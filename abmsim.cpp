#include <iostream> 
#include <string>
#include <vector>
#include <sstream>
#include "Agent.h"
#include "GridOfAgents.h"
#include "LocalNeighbourhood.h"
#include "frequency.h"
#include "PseudoRandomNumberGenerator.h"
#include "Cmdline.h"
#ifdef __cplusplus__
  #include <cstdlib>
#else
  #include <stdlib.h>
#endif
#include <unistd.h>

//this function initializes the parser for cmd options
cmdline::parser* initCmdlineParser(int argc, char* argv[]){
	cmdline::parser* CmdlineParserInstance = new cmdline::parser();

	CmdlineParserInstance->add<std::string>("init", 'i', "shape of initial opinion distributon",
											true, "", cmdline::oneof<std::string>("circle", "halfhalf", "random"));
	CmdlineParserInstance->add<std::string>("nMask", 'M', "type of local neighbor mask", 
											true, "", cmdline::oneof<std::string>("circle", "square"));
	CmdlineParserInstance->add("print", 0, "display every mod(grid**2) iteration");
	CmdlineParserInstance->add<int>("nRadius",'d',"local neighbor radius",false,1,cmdline::range(0,500));
	CmdlineParserInstance->add<int>("nRandom",'r',"number of random neighbors",false,0,cmdline::range(0,500));
	CmdlineParserInstance->add<int>("gSize",'g',"grid edge size",false,50,cmdline::range(1,1000));
	CmdlineParserInstance->add("help", 0, "print this message");
	CmdlineParserInstance->set_program_name("AbmSim");

	// try to parse the options. If it does not work, print some help.
	bool ok=CmdlineParserInstance->parse(argc, argv);

	if (argc==1 || CmdlineParserInstance->exist("help")){
		std::cerr<<CmdlineParserInstance->usage();
		return NULL;
	}
	if (!ok){
		std::cerr << CmdlineParserInstance->error()<<std::endl<<CmdlineParserInstance->usage();
		return NULL;
	}
	if ( CmdlineParserInstance->rest().size() > 0 ){ 
		std::cerr<<"Unrecognized options given."<<std::endl;
		std::cerr<<CmdlineParserInstance->usage();
		return NULL;
	}

	return CmdlineParserInstance;
}

int main(int argc, char* argv[]) {

	unsigned int delay_us = 40000;
	//std::cout <<" Hallo abmsim C++ go go go !\n";

	cmdline::parser* MyCmdlineParser = initCmdlineParser(argc,argv);
	if(!MyCmdlineParser){
		return 1;
	}
	
	std::string Initialization	= MyCmdlineParser->get<std::string>("init");
	std::string NeighborMask 	= MyCmdlineParser->get<std::string>("nMask");
	bool Print		 			= MyCmdlineParser->exist("print") ? true : false;
	int NeighborRadius			= MyCmdlineParser->get<int>("nRadius");
	int NeighborRandom			= MyCmdlineParser->get<int>("nRandom");
	int GridSize				= MyCmdlineParser->get<int>("gSize");
	bool dominance = false;
	double last_opinion_fraction = -1.0;
	int last_time_step = 0;


	const uint grid_edge = GridSize;
	const float local_radius = NeighborRadius;
	const uint time_steps = 1e8;
	const uint rand_neibor_size = NeighborRandom;

	PseudoRandomNumberGenerator prng;
	prng.set_seed_now_using_system_clock();
	
	GridOfAgents grid(grid_edge);
	
	std::vector<GridPosition> local_neibor_mask = ( NeighborMask == "circle" ? local_neighborhood::get_circle(local_radius) : 
													local_neighborhood::get_square(local_radius) );
		
	const uint local_neibor_size = local_neibor_mask.size();
	const uint comb_neiborhud_size = 
		local_neibor_size + rand_neibor_size;

	Agent** comb_neiborhud = new Agent*[comb_neiborhud_size];

	grid.set_rand_neighbourhood_size(rand_neibor_size);
	grid.set_local_neighbourhood_mask(local_neibor_mask);
	if( Initialization == "circle") grid.init_circle();
	else if ( Initialization == "halfhalf" ) grid.init_half_falf();
	else grid.init_random_fraction(prng.seed(), 0.5);

	for(uint time_step=0; time_step<time_steps; time_step++) {

		GridPosition rand_agent_pos = 
			grid.get_random_position_on_grid();

		grid.insert_local_neighbours_to_combined(
			rand_agent_pos,
			comb_neiborhud
		);

		grid.insert_random_neighbourhood(
			comb_neiborhud
		);

		float frac = 
			frequency::get_fraction_of_state_opponent_state_in_neighbourhood(
				comb_neiborhud, comb_neiborhud_size, 
				grid.get_agent_at(rand_agent_pos)->get_opinion()
			);

		if( frac >= prng.uniform() ) {
			grid.toggle_opinion_of_agent_at(rand_agent_pos);
		}

		if(time_step % (grid_edge*grid_edge) == 0 && Print) {
			double p1 = grid.get_opinion_fraction();
			if (system("CLS")) system("clear");
			std::cout << "time: " << time_step << ", local/random: " <<
				local_neibor_size << "/" << rand_neibor_size << " = " <<  
				float(local_neibor_size)/float(rand_neibor_size);
			
			std::cout << ", mean opinion: "<<p1;
			std::cout << "\n";
			std::cout << ", entropy: "<<(-1)*( p1*log(p1) + (1.0-p1)*log(1-p1));
			std::cout << "\n";
			std::cout << "Initialization: " << Initialization << ", NeighborMask: " << NeighborMask << "\n" ;
			std::cout << grid;

			usleep(delay_us);
		}

		if ( grid.get_opinion_fraction() == 1.0 || grid.get_opinion_fraction() == 0.0 ) {
			dominance = true;
			last_opinion_fraction = grid.get_opinion_fraction();
			last_time_step = time_step;
			break;
		}

	}

	//std::cout<< "#last_time_step,dominance,opinion_fraction "<<std::endl; 
	std::cout<< last_time_step << "," << dominance<< "," << last_opinion_fraction <<std::endl;
	return 0;
}