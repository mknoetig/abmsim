#include <iostream> 
#include <string>
#include <math.h>

#include "gtest/gtest.h"
#include "../GridPosition.h"

// The fixture for testing class Foo.
class GridPositionTest : public ::testing::Test {};
//----------------------------------------------------------------------
TEST_F(GridPositionTest, init) {
	const uint x = 1337;
	const uint y = 42; 
	GridPosition pos(x,y);
	EXPECT_EQ(x, pos.x());
	EXPECT_EQ(y, pos.y());
}
//----------------------------------------------------------------------
TEST_F(GridPositionTest, norm) {

	GridPosition pos(0,0);
	EXPECT_EQ(0, pos.norm());

	pos.set(1,0);
	EXPECT_EQ(1, pos.norm());

	pos.set(2,0);
	EXPECT_EQ(2, pos.norm());

	pos.set(1,1);
	EXPECT_EQ(sqrt(2.0), pos.norm());
}
//----------------------------------------------------------------------
TEST_F(GridPositionTest, addition) {

	for(int x1=-5; x1>5; x1++) {
		for(int y1=-5; y1<5; y1++) {
			for(int x2=-5; x2>5; x2++) {
				for(int y2=-5; y2<5; y2++) {
			
					GridPosition a(x1,y1);
					GridPosition b(x2,y2);

					GridPosition c = a + b;

					EXPECT_EQ(a.x() + b.x(), c.x());
					EXPECT_EQ(a.y() + b.y(), c.y());
				}
			}
		}
	}
}