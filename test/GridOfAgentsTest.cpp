#include <iostream> 
#include <string>
#include <math.h>

#include "gtest/gtest.h"
#include "../GridOfAgents.h"

// The fixture for testing class Foo.
class GridOfAgentsTest : public ::testing::Test {};
//----------------------------------------------------------------------
TEST_F(GridOfAgentsTest, init) {
  const int edge = 100;
  GridOfAgents grid(edge);

  for(uint row=0; row<edge; row++) {
    for(uint col=0; col<edge; col++) {
      GridPosition pos(row,col);
      EXPECT_NE(nullptr, grid.get_agent_at(pos));
    }
  }
}
//----------------------------------------------------------------------
TEST_F(GridOfAgentsTest, random_pos) {
  
  const int edge = 100;
  GridOfAgents grid(edge);

  for(uint i=0; i<edge*edge; i++) {
    GridPosition pos = grid.get_random_position_on_grid();
    EXPECT_TRUE(pos.x() >= 0);
    EXPECT_TRUE(pos.x() < edge);
    EXPECT_TRUE(pos.y() >= 0);
    EXPECT_TRUE(pos.y() < edge);
  }
}
