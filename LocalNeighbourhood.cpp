#include "LocalNeighbourhood.h"

namespace local_neighborhood {

	std::vector<GridPosition> get_circle(const float radius) {
		std::vector<GridPosition> circ_neighbours;

		std::vector<GridPosition> square_neighbours = get_square(int(radius));

		for(GridPosition pos : square_neighbours)
			if(pos.norm() <= radius)
				circ_neighbours.push_back(pos);

		return circ_neighbours;
	}

	std::vector<GridPosition> get_square(const int radius) {
		
		std::vector<GridPosition> square_neighbours;
		for(int x=-radius; x<radius+1; x++) {
			for(int y=-radius; y<radius+1; y++) {
				if(x==0 && y==0) {
					//neclegt yourself
				}else {
					GridPosition pos(x,y);
					square_neighbours.push_back(pos);
				}
			}
		}
		return square_neighbours;
	}
}
