//=================================
// include guard
#ifndef __AGENT_H_INCLUDED__
#define __AGENT_H_INCLUDED__

//=================================
// forward declared dependencies

//=================================
// included dependencies
#include <string>
#include <sstream>
//=================================
class Agent {

	int opinion;
public:

	void set_opinion(const int _opinion) {
		opinion = _opinion;
	}

	int get_opinion()const {
		return opinion;
	}

	std::string get_print()const {
		std::stringstream out;
		out << "Agent opinion: " << opinion;
		return out.str();
	}

	friend std::ostream& operator<<(std::ostream& os, const Agent& agent){
   	 	os << agent.get_print();
    	return os;
	};
};
#endif // __AGENT_H_INCLUDED__ 
