//=================================
// include guard
#ifndef __GRIDOFAGENTS_H_INCLUDED__
#define __GRIDOFAGENTS_H_INCLUDED__

//=================================
// forward declared dependencies

//=================================
// included dependencies
#include <string>
#include <sstream>
#include "Agent.h"
#include "GridPosition.h"
#include "PseudoRandomNumberGenerator.h"
//=================================
class GridOfAgents {

	Agent*** grid;
	const uint edge_length;

	PseudoRandomNumberGenerator prng;

	uint local_neibor_size;
	uint rand_neibor_size;
	std::vector<GridPosition> local_neighbourhood_mask;
public:

	GridOfAgents(const uint _edge_length);

	void init_half_falf();
	void init_random_fraction(const int, const double);

	void set_local_neighbourhood_mask(const std::vector<GridPosition> _local_mask) {
		local_neighbourhood_mask = _local_mask;
		local_neibor_size = local_neighbourhood_mask.size();
	};

	void set_rand_neighbourhood_size(const uint _rand_neibor_size) {
		rand_neibor_size = _rand_neibor_size;
	};

	Agent* get_agent_at(const GridPosition pos);

	Agent* get_random_agent();

	void insert_local_neighbours_to_combined(
		GridPosition &agent_position,
		Agent** &combined_neighbourhood
	);

	void insert_random_neighbourhood(
		Agent** &combined_neighbourhood
	); 

	void toggle_opinion_of_agent_at(const GridPosition pos);

	std::vector<GridPosition> get_all_positions()const;
	GridPosition get_random_position_on_grid();
	std::string get_print()const;
	double get_opinion_fraction();

	void init_circle();
	friend std::ostream& operator<<(std::ostream& os, const GridOfAgents& goa);
protected:
	int periodic_boundary(const int pos)const;
	void init_agents_on_grid();
};
#endif // __GRIDOFAGENTS_H_INCLUDED__ 
