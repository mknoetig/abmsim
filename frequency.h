//=================================
// include guard
#ifndef __frequency_H_INCLUDED__
#define __frequency_H_INCLUDED__

//=================================
// forward declared dependencies

//=================================
// included dependencies
#include <string>
#include <sstream>
#include <vector>
#include "Agent.h"
//=================================
namespace frequency {
	float get_fraction_of_state_opponent_state_in_neighbourhood(
		Agent** &neighbourhood, const uint size, const int my_state
	) {
		float sum_of_oponent_state = 0;
		for(uint i=0; i<size; i++)
			if(neighbourhood[i]->get_opinion() != my_state)
				sum_of_oponent_state++;

		return sum_of_oponent_state/size;
	}
}

#endif // __frequency_H_INCLUDED__ 
