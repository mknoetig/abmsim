//=================================
// include guard
#ifndef __LocalNeighbourhoodMaskGenerator_H_INCLUDED__
#define __LocalNeighbourhoodMaskGenerator_H_INCLUDED__

//=================================
// forward declared dependencies

//=================================
// included dependencies
#include <string>
#include <sstream>
#include "GridPosition.h"
#include <vector>

//=================================
namespace local_neighborhood {
	std::vector<GridPosition> get_circle(const float radius);
	std::vector<GridPosition> get_square(const int radius);
}
#endif // __LocalNeighbourhoodMaskGenerator_H_INCLUDED__ 
