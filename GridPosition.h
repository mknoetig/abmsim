//=================================
// include guard
#ifndef __GridPosition_H_INCLUDED__
#define __GridPosition_H_INCLUDED__

//=================================
// forward declared dependencies

//=================================
// included dependencies
#include <string>
#include <sstream>
#include <math.h>

//=================================
class GridPosition {

	int X;
	int Y;
public:
	GridPosition(const int _x, const int _y) {
		set(_x,_y);
	};
	
	void set(const int _x, const int _y) {
		X = _x;
		Y = _y;		
	};

	int x()const {
		return X;
	}

	int y()const {
		return Y;
	}

	double norm()const {
		return hypot(double(X),double(Y));
	}

	GridPosition operator+(const GridPosition pos)const {
		return GridPosition(X+pos.X, Y+pos.Y);
	}

	GridPosition operator-(const GridPosition pos)const {
		return GridPosition(X-pos.X, Y-pos.Y);
	}

	std::string get_print()const {
		std::stringstream out;
		out << "(" << X << " " << Y << ")";
		return out.str();
	}

	friend std::ostream& operator<<(std::ostream& os, const GridPosition& pos){
   	 	os << pos.get_print();
    	return os;
	};
};
#endif // __GridPosition_H_INCLUDED__ 
