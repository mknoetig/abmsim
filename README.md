# README #

This repository is hosting the project of a small group within the FS2015 ETH Zurich "Agent based modeling of social systems" course. The project team is: 

* Sebastian Müller
* Max Ahnen
* Patrik Friedlos
* Alexandre Caron

### What is this repository for? ###

* Agent based voter model 
* We are planning to investigate the neighborhood size and NN neighborhood/random neighborhood mixture models

### How do I get set up? ###

**Python**

still to be done

**C++ dependencies**:

If on Ubuntu (14.04), you can install the dependencies using synaptic, just install these:

* libgtest-dev

gtest is a header only library, but in order to use it one needs to build it first. See this thread on askubuntu for details: http://askubuntu.com/questions/145887/why-no-library-files-installed-for-google-test


```
#!Bourne shell

cd /usr/src/gtest
sudo cmake .
sudo make
sudo mv libg* /usr/lib/
```

**compile**:

```
#!Bourne shell

mkdir build
cd build
cmake ../AbmSim
make

```

**How to run tests**:

to be added...



### Contribution guidelines ###

* Write tests
* Keep it simple
* Do not try to build a flight simulator