# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 16:07:30 2015

@author: AlexandreCaron
"""

import numpy as np

def w(k, f):
    
    """
    this function takes in the non-linear response (k) and the frequencie f and return the transition rate w
    """
    return k(f)*f
    