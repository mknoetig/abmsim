# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 16:21:49 2015

@author: AlexandreCaron
"""

import numpy as np

def k(frequency, type_response, general_parameter, general_parameter2):
    
    """
    The general parameter are different depending the non linear response:
    
    -For the Normal distribution --> the general_parameter is how quickly the curves drops, the 
    general_parameter2 is not important    
    
    -For the Conspiracy --> the general_parameter is the probability of someone changing opinion within a almost
    uniform group and general_parameter2 is the frequency until the the curve becomes linear
    
    """
    if type_response == 'linear':
        return 1.0
        
    if type_response == 'Normal distribution':
        return (np.exp(-(general_parameter*(frequency-0.5))**2))/frequency
    
    if type_response == 'Minority_voting':
        return (2*frequency**3-3*frequency**2+1)/frequency
    
    if type_response == 'conspiracy':
        if frequency < general_parameter2:
            return general_parameter/frequency
        else:
            return 1.0
        
    
            
    
    