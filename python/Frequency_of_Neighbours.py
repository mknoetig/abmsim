# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 13:25:17 2015

@author: AlexandreCaron
"""

import numpy as np

def get_frequency_of_oppenents(local_neighbourhood, random_neighbourhood, index, matrix):
    
    """
    This function calculates the frequency of neighbours. This means that it calculates the number of "1" and "0"
    in the neighbourhood defined by another function.
    
    The function takes in the relative neighbourhood the random neighbourhood, the index in the matrix and the matrix itself.
    It returns the frequency of neighbours with opposite opinions.
    """
    neighbourhood = np.concatenate((local_neighbourhood, random_neighbourhood))
    
    numb_1 = 0
    numb_0 = 0
        
    
    for i in range(len(neighbourhood)):
       x = neighbourhood[i,0] + index[0]
       y = neighbourhood[i,1] + index[1]
       value = matrix[x%len(matrix),y%(len(matrix))]
       if value ==0:
           numb_0 = numb_0 + 1
       if value == 1:
           numb_1 = numb_1 + 1
           
           
    s = matrix[index[0], index[1]]
    f1=(1.0/(1.0+numb_0+numb_1))*(s+numb_1)
    
    if s ==0:
        return f1
    else:
        return 1.0-f1
 
    return 
    