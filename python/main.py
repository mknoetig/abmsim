"""
Agent Based Modelling of Social Systems
Semesterproject by

Patrik,
Max,
Alexandre,
Sebastian
"""
import numpy as np
from initialize import get_initialized_grid
from neighbourhoodLocal import get_neighourhood_local
from get_random_agent import get_random_agent
from neighbourhoodRandom import get_neighourhood_random
from w import w
from Frequency_of_Neighbours import get_frequency_of_oppenents
from k import k

#def main():
print ("Agent Based Modelling of Social Systems")
print ("Invastigations on local vs. random neighbourhood impacts and ")
print ("non linear opinion models")

grid_edge_size = 20
radius_of_local_neighbourhood = 1
time_steps = 100000
size_random_neighbourhood = 0

def kappa(x):
	return k(x,
              type_response='linear',
              general_parameter = 0, 
              general_parameter2 = 0)

grid_of_agents = get_initialized_grid(
	grid_edge_size,
	"circle",
	radius=0.25
)

local_neighbourhood_mask = get_neighourhood_local(
	radius=radius_of_local_neighbourhood,
	shape="circle"
)

for time_step in range(time_steps):
	random_agent_position = get_random_agent(grid_edge_size)
	
	random_neighbourhood_mask = get_neighourhood_random(
		size_random_neighbourhood,
		grid_edge_size
	)

	frequency_of_opponents = get_frequency_of_oppenents(
		local_neighbourhood_mask,
		random_neighbourhood_mask,
		random_agent_position,
		grid_of_agents
	)

	probability_for_agent_to_change = w(kappa,frequency_of_opponents)

	if probability_for_agent_to_change > np.random.random():
		grid_of_agents[random_agent_position[0],random_agent_position[1]] = \
		1 - grid_of_agents[random_agent_position[0],random_agent_position[1]]

	if time_step%250 == 0:
		for i in range(25):
			print ("\n")
		print ("time step", time_step)
		print (grid_of_agents)

#if __name__ == "__main__":
#    main()