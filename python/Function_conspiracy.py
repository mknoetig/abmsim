# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 11:30:56 2015

@author: AlexandreCaron
"""
import numpy as np

def conspiracy(prob, x, value_nei, numb_nei, opinion):
    
    number = np.random.random()
    percent_opinion_1 = (value_nei/numb_nei)
 
    if number < x:
        if opinion == 0:
            return 1-prob
        return prob
    
    if number > ((value_nei/numb_nei)-x):
        if opinion == 1:
            return 1-prob
        return prob
    
    slope_negative = (0-prob)/((percent_opinion_1/2)-x)
    b_negative = slope_negative*-1*(percent_opinion_1/2)
    
    slope_positive= slope_negative*-1
    b_positive = slope_positive*-1*(percent_opinion_1/2)
    
    
    if number < (percent_opinion_1/ 2):
        probability = slope_negative*number+b_negative
        return probability
    
    if number > (percent_opinion_1/ 2):
        probability = slope_positive*number+b_positive
        return probability
        

if __name__ == "__main__":
    a=0
    a = conspiracy(0.7, 0.1, 8, 16, 0)
    print(a)
    
        
        

            
        
        
        
    