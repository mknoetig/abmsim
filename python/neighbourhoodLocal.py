import numpy as np

def get_neighourhood_local(radius, shape):
	"""
	input:
	1st: radius in units of grid spacing
	2nd: shape of local neigbourhood. It can be either a 'circle' or a 'square'
	
	output:
	np.array of size(number of local neighbours, 2) in relative coordinates
	"""

	
	if shape == "circle":
		return get_circle_neighourhood(radius)
	elif shape == "square":
		return get_square_neighourhood(radius)
	else:
		raise NameError('shape must either be circle or square')

def get_circle_neighourhood(radius):
	list_of_neighbours_on_square = get_square_neighourhood(radius)

	list_of_neighbours_in_circle = []

	for row in list_of_neighbours_on_square:
		if np.linalg.norm(row) <= radius:
			list_of_neighbours_in_circle.append(row)

	return np.array(list_of_neighbours_in_circle)

def get_square_neighourhood(radius):
	
	list_of_neighbours = []

	for x in range(-radius, radius+1):
		for y in range(-radius, radius+1):
			if x == 0 and y == 0:
				continue
			list_of_neighbours.append([x,y])

	return np.array(list_of_neighbours)
