import numpy as np

def get_random_agent(quadratic_grid_edge_size):
	return np.random.randint(quadratic_grid_edge_size, size=(2))