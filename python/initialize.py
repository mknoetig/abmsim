# -*- coding: utf-8 -*-
"""
Created on Sat Apr 18 11:22:35 2015

@author: fpatrik
"""

def get_initialized_grid(edge_length, pattern, transition_length=-1,
                            radius=-1, fraction_of_state1=-1):
    """
    Initializes the grid of agents.
    
    Arguments:

    edge_length: the edge_length of the n*n matrix
    pattern: type of the geometrical initialization configuration

    pattern 'random':   The entries are randomly initialized to either 1 or 0.
                        fraction_of_state1 takes the desired fraction of 1.
                        
    pattern 'half':     The upper half of the array is initialized to 0, 
                        the rest is 1.
    
    pattern 'softhalf': The upper half of the array is initialized to 0, 
                        the rest is 1. The transition ist smoothed out 
                        and transition_length gives the length of the transition 
                        as fraction of the size of the array.
                        
    pattern 'circle':   Creates a circle with the radius defined in the 
                        'radius' option.
    
    pattern 'softcircle':   Creates a circle with the radius defined in the 
                            'radius' option. transition_length defines how quickly 
                            the probability of state 1 decreases with radius.
    
    radius: the fraction of the grid's edge length that the circle will have.

    Output:
    np.array of size(edge_length,edge_length) initialized to either 0 or 1 according to options

    """
    import numpy as np
    
    m=np.zeros((edge_length, edge_length),dtype=int)
    
    if pattern=='random':
        if fraction_of_state1 == -1:
            raise NameError('option random needs the fraction_of_state1 to be defined')

        for i in range(edge_length):
            for j in range(edge_length):
                m[i,j]=np.random.choice([0,1],p=[1.0-fraction_of_state1,fraction_of_state1])
    
        return m
    
    elif pattern=='half':
        for i in range(edge_length):
            for j in range(edge_length):
                if i<edge_length/2.0:
                    m[i,j]=1
        return m
    
    elif pattern=='softhalf':
        if transition_length == -1:
            raise NameError('option softhalf needs the transition_length to be defined')

        for i in range(edge_length):
            for j in range(edge_length):
                length=edge_length*transition_length-1
                if i<(edge_length-length)/2.0:
                    m[i,j]=1
                else:
                    if i< edge_length-(edge_length-length)/2.0:
                        m[i,j]=np.random.choice([0,1],p=[(i-((edge_length-length)/2.0))/length,1.0-(i-((edge_length-length)/2.0))/length])
                    
        return m
        
    elif pattern=='softcircle':
        if radius == -1:
            raise NameError('option softcircle needs the radius to be defined')
        if transition_length == -1:
            raise NameError('option softcircle needs the transition_length to be defined')

        for i in range(edge_length):
            for j in range(edge_length):
                half=edge_length/2.0
                distance=np.linalg.norm([i-half,j-half])
                if distance<radius*edge_length:
                    if np.power(1.0/distance,transition_length)>=1.0:
                        m[i,j]=1
                    else:
                        m[i,j]=np.random.choice([0,1],p=[1.0-np.power(1.0/distance,transition_length),np.power(1.0/distance,transition_length)])
        return m
        
    elif pattern=='circle':
        if radius == -1:
            raise NameError('option circle needs the radius to be defined')

        for i in range(edge_length):
            for j in range(edge_length):
                half=edge_length/2.0
                distance=np.linalg.norm([i-half,j-half])
                if distance<radius*edge_length:
                    m[i,j]=1
        return m
        
    raise NameError('allowed patterns: random, half, soft, circle','softcircle')