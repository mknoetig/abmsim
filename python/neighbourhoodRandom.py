import numpy as np

def get_neighourhood_random(size_of_random_neighbourhood, quadratic_grid_edge_size):
	"""
	input:
	1st: size_of_random_neighbourhood
	2nd: quadratic_grid_edge_size

	output:
	np.array with size (size_of_random_neighbourhood,2)
	"""
	return np.random.randint(quadratic_grid_edge_size, 
							size=(size_of_random_neighbourhood,2))